package com.phault.artemis.jumpstart;

import com.artemis.*;
import com.artemis.link.EntityLinkManager;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.bitfire.postprocessing.PostProcessor;
import com.bitfire.postprocessing.effects.Antialiasing;
import com.bitfire.postprocessing.effects.Fxaa;
import com.bitfire.utils.ShaderLoader;
import com.kotcrab.vis.ui.VisUI;
import com.phault.artemis.essentials.hierarchy.systems.HierarchyManager;
import com.phault.artemis.essentials.scenegraph.systems.WorldTransformationManager;
import com.phault.artemis.essentials.systems.CameraSystem;
import com.phault.artemis.essentials.systems.InputSystem;

public class Game extends ApplicationAdapter {

    private World world;
    private PostProcessor postProcessor;

    private final Color clearColor = new Color(0.07f, 0.07f, 0.07f, 1);

    @Override
	public void create () {

        VisUI.load(VisUI.SkinScale.X2);

        InputSystem inputSystem = new InputSystem();
        Gdx.input.setInputProcessor(inputSystem);

        WorldConfigurationBuilder builder = new WorldConfigurationBuilder()
                .with(new EntityLinkManager())
                .with(inputSystem)
                .with(new HierarchyManager())
                .with(new WorldTransformationManager());

        WorldConfiguration config = builder.build();

        world = new World(config);

        boolean isDesktop = Gdx.app.getType() == Application.ApplicationType.Desktop;
        ShaderLoader.BasePath = "shaders/";
        postProcessor = new PostProcessor(false, false, isDesktop);
        postProcessor.setClearColor(clearColor);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void resume() {
        super.resume();
        postProcessor.rebind();
    }

    @Override
	public void render () {
        Gdx.gl20.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        postProcessor.capture();

        world.setDelta(Gdx.graphics.getDeltaTime());
        world.process();

        postProcessor.render();
	}
	
	@Override
	public void dispose () {
        world.dispose();
        postProcessor.dispose();
        VisUI.dispose();
	}
}
