# libgdx-artemis-jumpstart
A starting point for libGDX projects using artemis.

### Tech
* [libGDX] - Game development framework
* [Artemis-odb] - Entity-Component-System framework
* [libgdx-contribs] - A fork of [manuelbua's post-processing library](https://github.com/manuelbua/libgdx-contribs)
* [libgdx-artemis-essentials] - A collection of useful artemis components and systems
* [VisUI] - UI Library

### License
Apache License Version 2.0

[libGDX]: <https://libgdx.badlogicgames.com/>
[Artemis-odb]: <https://github.com/junkdog/artemis-odb>
[libgdx-contribs]: <https://gitlab.com/casper.developer/libgdx-contribs>
[libgdx-artemis-essentials]: <https://gitlab.com/casper.developer/libgdx-artemis-essentials>
[VisUI]: <https://github.com/kotcrab/vis-editor/wiki/VisUI>